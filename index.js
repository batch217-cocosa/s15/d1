console.log("Mabuhay!");


//JS - is a loosely type Programming Language



// alert("Hello again!");

console. log  (   "Hello World"  )    ;

console.

log

(


"Hello World"

	)

;


// 
/**/





let myVariable;


console.log(myVariable);

let mySecondVariable = 2;

console.log(mySecondVariable);

let x = "desktop comp";

console.log(x);





let friend;


friend = "kate";
friend = "jane";
friend = "kris"
console.log(friend);



const outerExample = "Global Variable"
{
	const innerExample = "Local Variable" 
	console.log (innerExample);
}

 console.log(outerExample);


 // Multiple Declaration 

 let productCode = "DC017" , productBrand = "Dell"

 console.log (productCode, productBrand);

 let country = 'Philippines';
 let province = 'Metro Manila';

 // Concatenating Strings
 // Multiple string values can be combined to create a single string using the "+" symbol

 let fulladdress = province + "," + country;

 console.log (fulladdress);

 // the escape character (\) in strings in combination with the other characters can produce different effects

 let mailaddress = "Metro Manila\n\nPhilippines"
 console.log(mailaddress);


let message = "John's employees went home early.";
console.log(message)


let grade = 98.7;
console.log (grade);


let planetdistance = 2e10;

console.log(planetdistance);


let isMarried = false;
let inGoodConduct = true;

console.log ("isMarried" + isMarried);
console.log ("inGoodConduct" + inGoodConduct);


//Arrays
let grade1 = [98.7, 92.1, 90.2, 94.6]
console.log(grade1);

// Array with diff data types
let details = ["John", "Smith", 32, true];
console.log(details);


// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items

// Syntax

let person = {
	fullname: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contacts: ["09123456789","03333339999"],
	address: {
		houseNumber:'345',
		city: 'Manila'
	}
}

console.log(person);



// Re-assigning value to an array

const anime = ["OP","OPM","AOT"]
anime[0]=["KNY"];

console.log(anime)